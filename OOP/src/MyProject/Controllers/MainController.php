<?php
    namespace MyProject\Controllers;//использование пространств имён
    use MyProject\Models\Articles\Article;
    use MyProject\View\View;

    class MainController{
        private $view;// Неопределён
        private $db;// Неопределён Неисправимая ошибка

        public function __construct(){// Объявление общедоступного конструктора
            $this->view = new View(__DIR__.'/../../../templates');
        }
        public function main(){// Объявление общедоступного метода
            $articles = Article::findAll();//Оператор разрешения области видимости
            $this->view->renderHtml('main/main.php', ['articles' => $articles]);

        }
        public function sayHello(string $name){// Объявление общедоступного метода
            $this->view->renderHtml('main/hello.php', ['name' => $name]);

        }
    }
?>