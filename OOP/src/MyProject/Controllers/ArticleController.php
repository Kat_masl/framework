<?php
    namespace MyProject\Controllers;//использование пространств имён
    use MyProject\Models\Articles\Article;
    use MyProject\Models\Users\User;
    use MyProject\View\View;

    class ArticleController{
        private $view;// Неопределён
        private $db;// Неопределён

        public function __construct(){// Объявление общедоступного конструктора
            $this->view = new View(__DIR__.'/../../../templates');
        }
        public function view(int $articleId){// Объявление общедоступного метода
            $article = Article::getById($articleId);
            $reflector = new \ReflectionObject($article);
            $properties = $reflector->getProperties();
            $propertiesName = [];
            foreach($properties as $property){//перебора массивов. foreach работает только с массивами и объектами, и будет генерировать ошибку при попытке использования с переменными других типов или неинициализированными переменными.
                $propertiesName[] = $property->getName(); 
            }
            // var_dump($propertiesName);
            if ($article === null){
                $this->view->renderHtml('errors/404.php', [], 404);
                return;
            }
            $this->view->renderHtml('articles/view.php', ['article' => $article]);
        }

        public function edit(int $articleId): void// Объявление общедоступного метода
        {
            $article = Article::getById($articleId);//Оператор разрешения области видимости
            if ($article === null){
                $this->view->renderHtml('errors/404.php', [], 404);
                return;
            }
            $article->setName('New title');
            $article->setText('New text');
            $article->save();
        }
        public function add(): void{// Объявление общедоступного метода
            $author = User::getById(1);//Оператор разрешения области видимости
            $article = new Article();
            $article->setAuthorId($author);
            $article->setName('new title 07');
            $article->setText('new text 07');
            $article->save();
        }
        public function delete(int $articleId):void{
            $article = Article::getById($articleId);//
            if ($article === null){
                $this->view->renderHtml('errors/404.php', [], 404);
                return;
            }
            $article->delete();
        }
    }
?>