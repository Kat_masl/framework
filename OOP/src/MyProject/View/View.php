<?php
    namespace MyProject\View;//использование пространств имён

    class View{
        private $templatesPath;//Неопределен

        public function __construct(string $templatesPath){// Объявление общедоступного конструктора
            $this->templatesPath = $templatesPath;
        }

        public function renderHtml(string $templateName, array $vars = [], int $code = 200){
            extract($vars);
            http_response_code($code);
            include $this->templatesPath.'/'.$templateName;

        }
    }
?>