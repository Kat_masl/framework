<?php
    namespace MyProject\Models\Articles;//использование пространств имён
    use MyProject\Models\Users\User;
    use MyProject\Models\ActiveRecordEntity;

    class Article extends ActiveRecordEntity{
        protected $name;// Неисправимая ошибка
        protected $text;
        protected $authorId;
        protected $createdAt;

        public static function getTableName(): string{
            return 'articles';
        }
        public function getText(){// Объявление общедоступного метода
            return $this->text;
        }public function getName(){// Объявление общедоступного метода
            return $this->name;
        }
        public function setName(string $name){// Объявление общедоступного метода
            $this->name = $name;
        }
        public function setText(string $text){// Объявление общедоступного метода
            $this->text = $text;
        }
        public function setAuthorId(User $author){// Объявление общедоступного метода
            $this->authorId = $author->id;
        }
    }
?>