<?php
    namespace MyProject\Models\Users;//использование пространств имён
    use MyProject\Models\ActiveRecordEntity;

    
    class User extends ActiveRecordEntity{//Определение User
        private $name;// Неопределён

        public function getName(){// Объявление общедоступного метода
            return $this->name;
        }
        public static function getTableName(): string{// Объявление статического метода
            return 'users';
        }
    }
?>