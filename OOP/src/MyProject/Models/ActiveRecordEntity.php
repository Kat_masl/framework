<?php
    namespace MyProject\Models;//использование пространств имён
    use MyProject\Services\Db;

    abstract class ActiveRecordEntity{// абстрактный метод
        protected $id;// Неисправимая ошибка
        public function __set($name, $value){//Метод __set() будет выполнен при записи данных в недоступные (защищённые или приватные) или несуществующие свойства.
            $camelCase = $this->underscoreToCamelCase($name);
            $this->$camelCase = $value;
        }
        private function underscoreToCamelCase(string $source):string// Объявление общедоступного метода
        {
            return lcfirst(str_replace('_', '',ucwords($source, '_')));
        }
        private function camelCaseToUnderScore(string $source): string// Объявление общедоступного метода
        {
            return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $source));
        }

        public static function findAll(): array// Объявление статического метода
        {
            $db = Db::getInstance();
            return $db->query('SELECT * FROM `'.static::getTableName().'`', [], static::class);
        }
        public static function getById(int $id): ?self{// Объявление статического метода
            $db = Db::getInstance();
            $sql = 'SELECT * FROM `'.static::getTableName().'` WHERE id = :id';
            $article = $db->query($sql, [':id' => $id], static::class );
            return $article ? $article[0] : null;
        }

        public function save(): void
        {
            $mappedProperties = $this->mapPropertiesToDbFormat();
            if ($this->id !== null){
                $this->update($mappedProperties);
            } else{
                $this->insert($mappedProperties);
            }
        }

        public function update(array $mappedProperties): void
        {
            $columnsToParams = [];
            $paramsToValues = [];
            $index = 1;
            foreach($mappedProperties as $column => $value){
                $param = ':param'.$index;
                $columnsToParams[] = $column. '='.$param;
                $paramsToValues[$param] = $value; 
                $index++;
            }
            $sql = 'UPDATE `'.static::getTableName().'` SET '.implode(', ', $columnsToParams).' WHERE id = '.$this->id;
            $db = Db::getInstance();//Оператор разрешения области видимости
            $db->query($sql, $paramsToValues, static::class);
        }
        public function insert(array $mappedProperties): void
        {
            $filterMappedProperties = array_filter($mappedProperties);
            $columns = [];
            $params = [];
            $paramsToValues = [];
            foreach($filterMappedProperties as $column => $value){
                $columns[] ='`'.$column.'`';
                $paramsName = ':'.$column;
                $params[] = $paramsName;
                $paramsToValues[$paramsName] = $value;
            }
            $sql = 'INSERT INTO `'.static::getTableName().'` ('.implode(', ', $columns).') VALUES ('.implode(', ', $params).')';
            $db = Db::getInstance();//Оператор разрешения области видимости
            $db->query($sql, $paramsToValues, static::class);
        }

        public function delete():void{
            $db = Db::getInstance();//Оператор разрешения области видимости
            $db->query('DELETE FROM '.static::getTableName().' WHERE id = :id', [':id' => $this->id], static::class);
            $this->id = null;
        }

        private function mapPropertiesToDbFormat(): array
        {
            $reflector = new \ReflectionObject($this);
            $properties = $reflector->getProperties();

            $mappedProperties = [];
            foreach($properties as $property){//перебора массивов. foreach работает только с массивами и объектами, и будет генерировать ошибку при попытке использования с переменными других типов или неинициализированными переменными.
                $propertyName = $property->getName();
                $propertyNameUnderScore = $this->camelCaseToUnderScore($propertyName);
                $mappedProperties[$propertyNameUnderScore] = $this->$propertyName; 
            }
            return $mappedProperties;
        }
        abstract public static function getTableName(): string;
        
        public function getId(){
            return $this->id;
        }
    }
?>