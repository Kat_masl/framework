<?php
//Делаем константы для хранения данных о базе данных
//HOST - адрес для подключения к БД
//USER - логин для доступа к БД
//PASSWORD - пароль для доступа к БД
//DATABASE - название базы данных, к которой мы подключаемся

    
define('HOST', 'localhost');
define('USER', 'root');
define('PASSWORD', '');
define('DATABASE', 'db-blog');
    
//Подключаемся к базе данных с помощью функции mysqli_connect()

$connect = mysqli_connect(HOST, USER, PASSWORD, DATABASE);
    
//Делаем проверку соединения
//Если есть ошибки, останавливаем код и выводим сообщение с ошибкой

if (!$connect) {
    die('Error connect to database!');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>Chat</title>
    <div class="col-12 d-flex flex-column align-items-center">
        <h1>Комментарии</h1>
            <?php
            //Делаем выборку всех строк из таблицы "sms"
            $cards = mysqli_query($connect, "SELECT * FROM `message`");
            //Преобразовываем полученные данные в нормальный массив
            $cards = mysqli_fetch_all($cards);
            // Перебираем массив и рендерим HTML с данными из массива
            foreach ($cards as $card) {
            ?>
            <div class="card mb-3 col-5 ">
                <div class="card-header">
                    Комментарий
                </div>
                <div class="card-body">
                    <p class="card-text">Номер: <?= $card[0] ?></p>
                    <h5 class="card-title">Имя: <?= $card[1] ?></h5>
                    <p class="card-text">Дата: <?= $card[2] ?></p>
                    <p class="card-text">Сообщение: <?= $card[3] ?></p>
                    <a href="update.php?id=<?= $card[0] ?>" class="btn btn-dark">Изменить</a>
                </div>
            </div>
            <?php
            }
            ?>
      
        <a class="btn btn-danger" href="/322/OOP/www/article/1" role="button">Назад</a>
  
        </div>
    </div>
<head>
</html>
