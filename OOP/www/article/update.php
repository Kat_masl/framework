<?php
//Делаем константы для хранения данных о базе данных
//HOST - адрес для подключения к БД
//USER - логин для доступа к БД
//PASSWORD - пароль для доступа к БД
//DATABASE - название базы данных, к которой мы подключаемся

    
define('HOST', 'localhost');
define('USER', 'root');
define('PASSWORD', '');
define('DATABASE', 'db-blog');
    
//Подключаемся к базе данных с помощью функции mysqli_connect()

$connect = mysqli_connect(HOST, USER, PASSWORD, DATABASE);
    
//Делаем проверку соединения
//Если есть ошибки, останавливаем код и выводим сообщение с ошибкой

if (!$connect) {
    die('Error connect to database!');
}

//Получаем ID продукта из адресной строки - /product.php?id=1

$card_id = $_GET['id'];

//Делаем выборку строки с полученным ID выше

$product = mysqli_query($connect, "SELECT * FROM `message` WHERE `id` = '$card_id'");

//Преобразовывем полученные данные в нормальный массив Используя функцию mysqli_fetch_assoc массив будет иметь ключи равные названиям столбцов в таблице

$product = mysqli_fetch_assoc($product);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>update</title>
</head>
<body>
    <div class="col-12 d-flex flex-column align-items-center m-3">
        <h1 class="text-center">Изменить комментарий</h1>
        <form action="update1.php" class="d-flex flex-column nx-2 flex-5 col-4" name="SMS-form" method="post">
        <input type="hidden" name="id" value="<?= $product['id'] ?>">
            <div class="mb-3">
                <label class="nx-2" for="canal" class="form-label">Автор</label>
                <input name="name" type="text" class="form-control n-2" id="canal" value="<?= $product['name'] ?>" placeholder="Введите свой никнейм" required>
            </div>
            <div class="mb-3">
                <label class="nx-2" for="text" class="form-label">Сообщение</label>
                <input name="content" type="text" class="form-control n-2" value="<?= $product['content'] ?>" id="text" placeholder="Введите сообщение" required>
            </div>
            <div class="d-grid gap-2 col-12 mx-auto">
                <button class="btn btn-dark" type="submit">Изменить</button>
                <a class="btn btn-danger" href="comment.php" role="button">Не хочу изменять</a>
            </div>
        </form>
    </div>
</body>